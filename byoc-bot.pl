use strict;
use warnings;

use Irssi;
use LWP::Simple;
use JSON;

our $VERSION = '2.14';
our %IRSSI = (
	authors		=> 'Keith "blitter" Kaisershot',
	contact		=> 'keithkaisershot@gmail.com',
	name		=> 'BYOC Bot',
	description	=> 'Provides BYOC lookup services.',
	license		=> 'MIT'
);

# changelist:
#
#  2.14:
#   - Changed to using cdn.seats.io URL (and API) for seat layout.
#   - Updated seats.io URL with event ID for QuakeCon 2019.
#  2.13:
#   - 'stats' command now counts booked/reserved/unavailable seats rather than all non-free seats.
#     This mitigates stats fluctuations due to seats being held/released.
#  2.12:
#   - Updated and re-enabled byoc.ninja integration.
#   - Re-enabled the 'nick' command, renamed it 'seat.'
#   - Renamed the original 'seat' command to 'find.'
#  2.11:
#   - Added 'stats' command.
#   - Fixed bug that would report a non-existent seat as taken.
#  2.10:
#   - Migrated to seats.io v2 API.
#   - Updated seats.io URL with public key and event ID for QuakeCon 2018.
#   - Disabled nick/clan/game searches since that info isn't available yet.
#  2.05:
#   - Added support for qc_discord bot.
#  2.04:
#   - Re-enabled byoc.ninja integration.
#  2.03:
#   - Temporarily disabled byoc.ninja integration.
#  2.02:
#   - Updated seats.io URL with event ID for QuakeCon 2017.
#  2.01:
#   - Updated byoc.ninja URL.
#  2.00:
#   - Added 'game' command with byoc.ninja integration.
#   - Refactored and cleaned up a lot of the code.
#  1.11:
#   - Fixed bug where args were trimmed before checking for null.
#  1.10:
#   - Added 'nick' command.
#   - Do exact matches for nicks and seats.
#  1.00:
#   - Initial version.

#############
# constants #
############################################################
# you shouldn't have to modify anything after this section #
############################################################

my $totalSeats = 3289;	# total available seats in the BYOC (to be compared against seats claimed)
my $seatsioEventKey = 5271915;	# event key for calling seats.io API
my $byocninjaAuthToken = $ENV{'BYOC_NINJA_AUTH_TOKEN'};	# token for calling byoc.ninja API

my $seatsioDefinitionsJsonURL = 'https://cdn.seatsio.net/static/version/v361/charts/bf8f9d39-d8a4-4743-ae3e-3926b4f96dc0/781574e9-ca44-4f5f-a55f-f2036635e629/487';
my $seatsioStatusesJsonURL = 'https://api.seatsio.net/system/public/bf8f9d39-d8a4-4743-ae3e-3926b4f96dc0/events/object-statuses?event_key=';
my $byocninjaSeatsJsonURL = 'https://byoc.ninja/api/v1/seats';
my $byocninjaHostsJsonURL = 'https://byoc.ninja/api/v1/hosts';

###########
# globals #
###########

my %seatNameHash = ();	# local seat name lookup table

my %seatHash = ();	# local seat lookup table
my %seatRevHash = ();	# reverse seat lookup table (for finding nick based on seat)
my $seatsClaimed = 0;	# seats that aren't marked "free"

my %gameHash = ();	# local game lookup table

#####################
# utility functions #
#####################

sub strip
{
	my ($str) = @_;
	
	if ($str)
	{
		$str =~ s/^\s+//;	# strip whitespace from beginning of string
		$str =~ s/\s+$//;	# strip whitespace from end of string
	}
	
	return $str;
}

sub strip_lc
{
	my ($str) = @_;
	
	if ($str)
	{
		$str = strip($str);
		$str = lc($str);
	}
	
	return $str;
}

sub strip_uc
{
	my ($str) = @_;
	
	if ($str)
	{
		$str = strip($str);
		$str = uc($str);
	}
	
	return $str;
}

#############################
# backend support functions #
#############################

# seats.io
sub fetch_seat_names
{
	# should only need to be called once, as seat names should never change
	
	my $json = get($seatsioDefinitionsJsonURL);
	my $decoded_json = decode_json($json);

	%seatNameHash = ();
	foreach my $row (@{$decoded_json->{'subChart'}->{'rows'}})
	{
		foreach my $seat (@{$row->{'seats'}})
		{
			$seatNameHash{ $seat->{'uuid'} } = substr($seat->{'categoryLabel'}, length 'SECTION ') . '-' . $row->{'label'} . '-' . $seat->{'label'};
		}
	}
}

sub fetch_seat_data
{
	my $json = get($seatsioStatusesJsonURL . $seatsioEventKey);
	my $decoded_json = decode_json($json);
	
	%seatRevHash = ();
	$seatsClaimed = 0;
	foreach my $seat (@{$decoded_json})
	{
		my $seatName = $seatNameHash{ $seat->{'objectLabelOrUuid'} };
		if ($seatName)
		{
			my $seatStatus = $seat->{'status'};
			$seatRevHash{ $seatName }{'status'} = $seatStatus;
			if ($seatStatus eq 'booked' || $seatStatus eq 'reserved' || $seatStatus eq 'unavailable')
			{
				++$seatsClaimed;
			}
		}
	}
}

# byoc.ninja
sub fetch_nick_data
{
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get($byocninjaSeatsJsonURL, 'Authorization' => 'Token token=' . $byocninjaAuthToken);
	my $json = $response->content;
	my $decoded_json = decode_json($json);
	
	%seatHash = ();
	foreach my $seat (@{$decoded_json})
	{
		my $seatName = $seat->{'seat'};
		if ($seatName)
		{
			my @seatUsers = @{ $seat->{'users'} };
			if (@seatUsers)
			{
				my $firstUser = $seatUsers[0];
				if ($firstUser)
				{
					$seatRevHash{ $seatName }{'nick'} = $firstUser->{'name'};
				}
				
				foreach my $seatUser (@seatUsers)
				{
					$seatHash{ $seatUser->{'name'} }{'seat'} = $seatName;
				}
			}
		}
	}
}

sub fetch_game_data
{
	my $ua = LWP::UserAgent->new;
	my $response = $ua->get($byocninjaHostsJsonURL, 'Authorization' => 'Token token=' . $byocninjaAuthToken);
	my $json = $response->content;
	my $decoded_json = decode_json($json);
	
	%gameHash = ();
	foreach my $game (@{$decoded_json})
	{
		my $gameInfo = { map => $game->{'map'}, game => $game->{'game'}->{'name'}, storeURL => $game->{'game'}->{'link'}, flags => $game->{'flags'}, server => $game->{'name'}, joinURL => $game->{'link'} };
		
		foreach my $user (@{ $game->{'users'} })
		{
			my $name = $user->{'name'};
			
			my @seats = @{ $user->{'seats'} };
			if (@seats)
			{
				my $seatName = $seats[0]->{'seat'};
				my $nick = $seatRevHash{ $seatName }{'nick'};
				if ($nick)
				{
					$name = $nick;
				}
			}
			
			$name =~ s/\^\d{1}//g;
			$gameHash{ $name } = $gameInfo;
		}
	}
}

###########################
# data accessor functions #
###########################

sub get_nick_info
{
	my ($nick) = @_;

	# This is gross, but whatever
	fetch_seat_data();
	fetch_nick_data();

	foreach my $key (keys %seatHash)
	{
		if ($key && length $key == length $nick && index(lc $nick, lc $key) != -1)
		{
			return $seatHash{$key};
		}
	}
}

sub get_seat
{
	my ($nick) = @_;
	$nick = lc($nick);
	my $info = get_nick_info($nick);

	if ($info)
	{
		return $info->{'seat'};
	}
}

sub get_clan
{
	my ($nick) = @_;
	$nick = lc($nick);
	my $info = get_nick_info($nick);

	if ($info)
	{
		return $info->{'clan'};
	}
}

sub get_seat_info
{
	my ($nick) = @_;
	
	fetch_seat_data();
	fetch_nick_data();
	
	foreach my $key (keys %seatRevHash)
	{
		if ($key && length $key == length $nick && index(lc $nick, lc $key) != -1)
		{
			return $seatRevHash{$key};
		}
	}
}

sub get_nick
{
	my ($seat) = @_;
	$seat = uc($seat);
	my $info = get_seat_info($seat);

	if ($info)
	{
		return $info->{'nick'};
	}
}

sub get_status
{
	my ($seat) = @_;
	$seat = uc($seat);
	my $info = get_seat_info($seat);

	if ($info)
	{
		return $info->{'status'};
	}
}

sub get_game_info
{
	my ($nick) = @_;
	
	fetch_seat_data();
	fetch_nick_data();
	fetch_game_data();
	
	foreach my $key (keys %gameHash)
	{
		if ($key && length $key == length $nick && index(lc $nick, lc $key) != -1)
		{
			return $gameHash{$key};
		}
	}
}

my %help_console = (
	"FIND" => "nick : Returns the given nick's seat in the BYOC",
#	"CLAN" => "nick : Returns the given nick's clan association",
	"SEAT" => "seat : Returns the nick of the person in the given seat",
	"GAME" => "nick : Returns the given nick's current game and URL",
	"STATS" => ": Returns the current BYOC ticket stats",
	"REFRESH" => ": Refreshes BYOC map data from the service",
);

my %help_private = (
	"FIND" => "nick : Returns the given nick's seat in the BYOC",
#	"CLAN" => "nick : Returns the given nick's clan association",
	"SEAT" => "seat : Returns the nick of the person in the given seat",
	"GAME" => "nick : Returns the given nick's current game and URL",
	"STATS" => ": Returns the current BYOC ticket stats",
);

sub get_help
{
	my ($subcmd, $help) = @_;
	
	if ($subcmd)
	{
		$subcmd = uc($subcmd);
		
		if (exists $$help{$subcmd})
		{
			return "$subcmd " . $$help{$subcmd};
		}
	}

	return "Available commands: " . join(', ', sort keys %$help);
}

#########################
# base command handlers #
#########################

sub handle_cmd_help_console
{
	my ($subcmd) = @_;

	return get_help($subcmd, \%help_console);
}

sub handle_cmd_help_private
{
	my ($subcmd) = @_;

	return get_help($subcmd, \%help_private);	
}

sub handle_cmd_find
{
	my ($nick) = @_;
	
	if (!$nick)
	{
		return "Nick not given";
	}
	else
	{
		$nick = strip($nick);

		my $seat = get_seat($nick);
		if (!$seat || length $seat == 0)
		{
			return "No seat found for $nick";
		}
		else
		{
			$seat = uc($seat);
			return "$nick is at seat $seat";
		}
	}
}

sub handle_cmd_seat
{
	my ($seatName) = @_;
	
	if (!$seatName)
	{
		return "Seat not given";
	}
	else
	{
		$seatName = strip_uc($seatName);
		
		my $nick = get_nick($seatName);
		if (!$nick || length $nick == 0)
		{
			my $status = get_status($seatName);
			if (!$status || length $status == 0)
			{
				return "$seatName not found";
			}
			elsif ($status eq 'free')
			{
				return "$seatName has not been claimed";
			}
			else
			{
				return "$seatName is taken";
			}
		}
		else
		{
			return "$nick is at seat $seatName";
		}
	}
}

sub handle_cmd_clan
{
	my ($nick) = @_;
	
	if (!$nick)
	{
		return "Nick not given";
	}
	else
	{
		$nick = strip($nick);

		my $clan = get_clan($nick);
		if (!$clan || length $clan == 0)
		{
			return "$nick is not a member of a clan";
		}
		else
		{
			return "$nick is a member of $clan";
		}
	}
}

sub handle_cmd_game
{
	my ($nick) = @_;
	
	if (!$nick)
	{
		return "Nick not given";
	}
	else
	{
		$nick = strip($nick);

		my $game_info = get_game_info($nick);
		if (!$game_info)
		{
			return "No game found for $nick";
		}
		else
		{
			my $result = "$nick is playing ";
			if ($game_info->{'map'})
			{
				$result .= "map " . $game_info->{'map'} . " in ";
			}
			$result .= $game_info->{'game'} . " ";
			if ($game_info->{'storeURL'})
			{
				$result .= "(" . $game_info->{'storeURL'} . ")";
			}
			if ($game_info->{'server'})
			{
				$result .= " on ";
				if ($game_info->{'flags'} && $game_info->{'flags'}->{'Password Protected'})
				{
					$result .= "private ";
				}
				if ($game_info->{'flags'} && $game_info->{'flags'}->{'Quakecon in Host Name'})
				{
					$result .= "QuakeCon ";
				}
				$result .= "server " . $game_info->{'server'};
			}
			if ($game_info->{'joinURL'})
			{
				$result .= " -- " . $game_info->{'joinURL'};
			}
		}
	}
}

sub handle_cmd_stats
{
	fetch_seat_data();
	my $seatsAvailable = $totalSeats - $seatsClaimed;
	my $fullPercentStr = sprintf("%.2f", ($seatsClaimed / $totalSeats) * 100);
	return "Seats available: $seatsAvailable, seats taken: $seatsClaimed, full: $fullPercentStr%";
}

##########################
# irssi command wrappers #
##########################

sub subcmd_find
{
	my ($data, $server, $witem) = @_;
	my $win = Irssi::active_win;

	$win->print(handle_cmd_seat($data));
}

sub subcmd_clan
{
	my ($data, $server, $witem) = @_;
	my $win = Irssi::active_win;

	$win->print(handle_cmd_clan($data));
}

sub subcmd_seat
{
	my ($data, $server, $witem) = @_;
	my $win = Irssi::active_win;

	$win->print(handle_cmd_nick($data));
}

sub subcmd_game
{
	my ($data, $server, $witem) = @_;
	my $win = Irssi::active_win;

	$win->print(handle_cmd_game($data));
}

sub subcmd_refresh
{
	fetch_byoc_data();
	Irssi::print("BYOC data refreshed");
}

sub subcmd_help
{
	my ($subcmd) = @_;

	Irssi::print("\n" . handle_cmd_help_console($subcmd), MSGLEVEL_CLIENTCRAP);
	Irssi::signal_stop();
}

sub subcmd_stats
{
	my $win = Irssi::active_win;
	
	$win->print(handle_cmd_stats());
}

my $BASE_CMD = 'BYOC';

sub subcmd_handler
{
	my ($data, $server, $item) = @_;
	$data = strip_lc($data);
	Irssi::command_runsub($BASE_CMD, $data, $server, $item);
}

sub subcmd_unknown
{
	subcmd_help();
}

##################################################
# irssi message handlers                         #
# we'll map commands to their functions          #
# this makes it easy to enable/disable commands  #
# also easy to add aliases by just doubling up   #
##################################################

my %cmds_console = (
	'FIND' => \&subcmd_find,
#	'CLAN' => \&subcmd_clan,
	'SEAT' => \&subcmd_seat,
	'HELP' => \&subcmd_help,
	'GAME' => \&subcmd_game,
	'STATS' => \&subcmd_stats,
	'REFRESH' => \&subcmd_refresh,
);

my %cmds_private = (
	'FIND' => \&handle_cmd_find,
#	'CLAN' => \&handle_cmd_clan,
	'SEAT' => \&handle_cmd_seat,
	'HELP' => \&handle_cmd_help_private,
	'GAME' => \&handle_cmd_game,
	'STATS' => \&handle_cmd_stats,
);

my %cmds_public = (
	'FIND' => \&handle_cmd_find,
#	'CLAN' => \&handle_cmd_clan,
	'SEAT' => \&handle_cmd_seat,
	'STATS' => \&handle_cmd_stats,
);

sub process_message
{
	my ($server, $msg, $target, $cmds) = @_;
	my ($cmd, $arg) = split(' ', $msg, 2);

	if ($cmd)
	{
		$cmd = uc($cmd);
	}

	if (exists $$cmds{$cmd})
	{
		$server->send_message($target, $$cmds{$cmd}->($arg), 0);
	}
	elsif (exists $$cmds{'HELP'})
	{
		$server->send_message($target, $$cmds{'HELP'}->($arg), 0);
	}
}

######################################
# private message handler            #
# allow for HELP command in privmsgs #
######################################

sub cmd_message_private
{
	my ($server, $msg, $nick, $hostmask, $channel) = @_;
	process_message($server, $msg, $nick, \%cmds_private);
}

##################################################
# public message handler                         #
# don't allow HELP in public chats to avoid spam #
##################################################

sub cmd_message_public
{	
	my ($server, $msg, $nick, $hostmask, $channel) = @_;
	my $pos = 1;
#	if (substr($msg, 0, $pos) eq '!' || (index(lc $nick, 'qc_discord') == 0 && $msg =~ /(^<[a-zA-Z_\-\[\]\\\^\{\}\|\`][a-zA-Z0-9_\-\[\]\\\^\{\}\|\`]*> !)/ && ($pos = length $1)))
	if (substr($msg, 0, $pos) eq '!' || (index(lc $nick, 'qc_discord') == 0 && index($msg, '> !') != -1 && ($pos = index($msg, '> !') + 3)))
	{
		process_message($server, substr($msg, $pos), $channel, \%cmds_public);
	}
}

###############
# irssi main! #
###############

fetch_seat_names();
fetch_seat_data();
fetch_nick_data();
fetch_game_data();

Irssi::signal_add('message private', \&cmd_message_private);
Irssi::signal_add('message public', \&cmd_message_public);

foreach my $key (keys %cmds_console)
{
	Irssi::command_bind("$BASE_CMD $key", $cmds_console{$key});
}
Irssi::command_bind($BASE_CMD, \&subcmd_handler);
Irssi::signal_add_first("default command $BASE_CMD", \&subcmd_unknown);
