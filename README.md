BYOC Bot
========

This script for irssi provides QuakeCon BYOC lookup services via the seats.io API used by the QuakeCon registration site. Additionally, chapebrone's byoc.ninja API is integrated for tracking games played while in the BYOC. Available under the MIT license.

Basic Use
---------

Public and private commands are supported, both via privmsg and locally through an irssi console.

`SEAT <nick>` - Returns the given nick's seat in the BYOC.  
`CLAN <nick>` - Returns the given nick's clan association as registered.  
`NICK <seat>` - Returns the nick of the person in the given seat as registered.  
`GAME <nick>` - Returns the given nick's current game and URL via the byoc.ninja service. (except public)  
`HELP` - Provides inline help describing the available commands. (except public)  
`REFRESH` - Refreshes the BYOC data from seats.io. (console only)

Maintenance
-----------

The seats.io URL is at the top of the `fetch_seat_data` function-- update this each year with a respective event ID. In the source code to the QuakeCon BYOC map page, look for a "publicKey" GUID and an "event" value within a JSON string. These form the middle part of the REST URL.

The byoc.ninja URL is at the top of the `fetch_game_data` function-- get this URL from chapebrone. If this service is down, the initial call to `fetch_game_data` along with the command mappings in `cmds_console` and `cmds_private` can be commented out to temporarily disable byoc.ninja integration.

TODO
----

- Locally cache the seats.io data so we're not pulling down the whole map every time there's a request.

Build Notes
-----------

This is written in Perl, mostly because irssi expects scripts written in Perl by default and because RegEx is more or less built-in.

Have fun.

Keith Kaisershot
